﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using WebApi.Models;

namespace WebApi
{
    public class MyDbContext : DbContext
    {
    public MyDbContext() : base("name = Angular")
        {
            Database.SetInitializer<MyDbContext>(null);
        }
        public IDbSet<Employee> employee { get; set; }
        public IDbSet<Login> login { get; set; }


    }
}