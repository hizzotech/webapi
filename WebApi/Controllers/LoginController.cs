﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WebApi.Models;


namespace WebApi.Controllers
{
    [EnableCors(origins: "http://localhost:4200", headers: "*", methods: "*")]
    public class LoginController : ApiController
    {
        private MyDbContext db = new MyDbContext();

        // POST: api/Login
        [ResponseType(typeof(Login))]
        public IHttpActionResult Login(Login login)
        {
            var logins = db.login.Where(s => s.Username == login.Username && s.Password == login.Password).SingleOrDefault();
            
            if (logins == null)
            {
                return NotFound();
            }
            else
            {
                var data = new { Id = logins.Id};
               
                return Json(data);
            }

        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
